#include <compass/map.hpp>
#include <compass/locate.hpp>

#include <iostream>

struct Greeter :
    compass::Service,
    compass::Locator<Greeter> {

  void Greet() {
    std::cout << "Hi" << std::endl;
  }

  // compass::Locator
  Greeter* Locate(compass::Tag<Greeter>) override {
    return this;
  }
};

void Greet() {
  compass::Locate<Greeter>().Greet();
}

int main() {
  // Service
  Greeter greeter;

  compass::Map().Add(greeter);

  Greet();

  return 0;
}
